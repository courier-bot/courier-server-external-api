# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.3'

# Allows HTTP requests.
gem 'faraday'

# NATS messaging queue.
gem 'nats'

# Application server.
gem 'puma'

# Server-side web application framework.
gem 'rails', '~> 5.2.3'

# sprockets 4.0.0 has a path traversal vulnerability (CVE-2018-3760).
# Upgrade to sprockets 4.0.0.beta8 or newer
gem 'sprockets', '4.0.0.beta8'

group :test do
  # Generates fake values.
  gem 'faker'

  # Allows for mutation testing. This gem is handled by bundler because it needs to know about
  # `rspec` and all other gem versions.
  gem 'mutant-rspec', require: false

  # Allows for general testing. All tests are in the `spec/` folder.
  gem 'rspec-rails'

  # Prevents all HTTP requests from leaving the tests.
  gem 'webmock'
end
