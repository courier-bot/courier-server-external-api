# frozen_string_literal: true

require 'nats/client'

RSpec.describe MessagesController, type: :request do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_messages).and_return(messages)

    allow(internal_api_service).to receive(:create_message).with(anything) do |attrs|
      message_1.merge(attrs).merge(
        'id' => '9cb82c33-48d0-4866-92c6-972cf3c7fe57',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      )
    end

    allow(internal_api_service).to receive(:retrieve_message)
      .with('10d574f1-b8ab-4d7f-a621-edfd895fa42a').and_return(message_2)

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('4187ec65-112a-4338-ac86-dcd60bb044d8').and_return(message_kind_1)

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('9f210315-92a3-47bc-81a6-167d640d4dfa').and_return(message_kind_2)

    allow(internal_api_service).to receive(:retrieve_message_kind_by_code)
      .with('message_kind_code_1').and_return(message_kind_1)

    allow(internal_api_service).to receive(:retrieve_deliveries_by_message_id)
      .with('6c5da6e8-6753-4407-a3e1-cc3e5c9740c1').and_return(deliveries_1)

    allow(internal_api_service).to receive(:retrieve_deliveries_by_message_id)
      .with('10d574f1-b8ab-4d7f-a621-edfd895fa42a').and_return(deliveries_2)

    allow(internal_api_service).to receive(:retrieve_deliveries_by_message_id)
      .with('a8a7b386-22b1-4142-816b-676a366cf28a').and_return(deliveries_3)

    allow(internal_api_service).to receive(:retrieve_deliveries_by_message_id)
      .with('9cb82c33-48d0-4866-92c6-972cf3c7fe57').and_return([])

    allow(internal_api_service).to receive(:retrieve_delivery_kind)
      .with('8e4fe658-9479-4454-950d-a6e1d1e9af0f').and_return(delivery_kind_1)

    allow(internal_api_service).to receive(:retrieve_delivery_kind)
      .with('9cb82c33-48d0-4866-92c6-972cf3c7fe57').and_return(delivery_kind_2)

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('3bcc64ff-d711-4444-81ec-5b50e1c41be4').and_return(delivery_method_1)

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('b7849251-4d73-4c70-af00-993fe057505c').and_return(delivery_method_2)

    allow(internal_api_service).to receive(:retrieve_delivery_fields_by_delivery_id)
      .with('9824ee55-b5a1-44e4-838c-c23f1396a5a0').and_return(delivery_fields_1)

    allow(internal_api_service).to receive(:retrieve_delivery_fields_by_delivery_id)
      .with('7f29aa77-6b3d-4dbf-997c-2abfb1a58511').and_return(delivery_fields_2)

    allow(internal_api_service).to receive(:retrieve_delivery_fields_by_delivery_id)
      .with('017812ee-310d-4410-afcd-ea5fec0e76f8').and_return(delivery_fields_3)
  end

  let(:instance) { described_class.new }

  let(:messages) do
    [
      message_1,
      message_2,
      message_3
    ]
  end

  let(:message_1) do
    {
      'id' => '6c5da6e8-6753-4407-a3e1-cc3e5c9740c1',
      'messageKindId' => '4187ec65-112a-4338-ac86-dcd60bb044d8',
      'targetMessageable' => 'target_messageable_1',
      'status' => 'unconfirmed',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z'
    }
  end

  let(:message_2) do
    {
      'id' => '10d574f1-b8ab-4d7f-a621-edfd895fa42a',
      'messageKindId' => '9f210315-92a3-47bc-81a6-167d640d4dfa',
      'targetMessageable' => 'target_messageable_2',
      'status' => 'delivered',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z'
    }
  end

  let(:message_3) do
    {
      'id' => 'a8a7b386-22b1-4142-816b-676a366cf28a',
      'messageKindId' => '9f210315-92a3-47bc-81a6-167d640d4dfa',
      'targetMessageable' => 'target_messageable_3',
      'status' => 'pending',
      'createdAt' => '2000-01-02T03:04:09Z',
      'updatedAt' => '2000-01-02T03:04:10Z'
    }
  end

  let(:message_kind_1) do
    {
      'id' => '4187ec65-112a-4338-ac86-dcd60bb044d8',
      'code' => 'message_kind_code_1',
      'adapterId' => 'bc790f46-c4ff-4329-aac4-74cb9142441f',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z',
      'deletedAt' => nil
    }
  end

  let(:message_kind_2) do
    {
      'id' => '9f210315-92a3-47bc-81a6-167d640d4dfa',
      'code' => 'message_kind_code_2',
      'adapterId' => '3eadbbc2-c703-4702-8bfc-c79c80b63885',
      'createdAt' => '2000-01-02T03:04:13Z',
      'updatedAt' => '2000-01-02T03:04:14Z',
      'deletedAt' => nil
    }
  end

  let(:deliveries_1) do
    [
      {
        'id' => '9824ee55-b5a1-44e4-838c-c23f1396a5a0',
        'messageId' => '6c5da6e8-6753-4407-a3e1-cc3e5c9740c1',
        'deliveryKindId' => '8e4fe658-9479-4454-950d-a6e1d1e9af0f',
        'externalId' => 'external_id_1',
        'status' => 'undelivered',
        'undeliveredReason' => 'undelivered_reason_1',
        'createdAt' => '2000-01-02T03:04:15Z',
        'updatedAt' => '2000-01-02T03:04:16Z'
      },
      {
        'id' => '7f29aa77-6b3d-4dbf-997c-2abfb1a58511',
        'messageId' => '6c5da6e8-6753-4407-a3e1-cc3e5c9740c1',
        'deliveryKindId' => '9cb82c33-48d0-4866-92c6-972cf3c7fe57',
        'externalId' => 'external_id_2',
        'status' => 'unconfirmed',
        'undeliveredReason' => nil,
        'createdAt' => '2000-01-02T03:04:17Z',
        'updatedAt' => '2000-01-02T03:04:18Z'
      }
    ]
  end

  let(:deliveries_2) do
    [{
      'id' => '017812ee-310d-4410-afcd-ea5fec0e76f8',
      'messageId' => '10d574f1-b8ab-4d7f-a621-edfd895fa42a',
      'deliveryKindId' => '8e4fe658-9479-4454-950d-a6e1d1e9af0f',
      'externalId' => 'external_id_3',
      'status' => 'delivered',
      'undeliveredReason' => nil,
      'createdAt' => '2000-01-02T03:04:19Z',
      'updatedAt' => '2000-01-02T03:04:20Z'
    }]
  end

  let(:deliveries_3) do
    []
  end

  let(:delivery_kind_1) do
    {
      'id' => '8e4fe658-9479-4454-950d-a6e1d1e9af0f',
      'code' => 'delivery_kind_code_1',
      'deliveryMethodId' => '3bcc64ff-d711-4444-81ec-5b50e1c41be4',
      'adapterId' => '0e9f40e1-c439-4555-b040-495220bfde77',
      'createdAt' => '2000-01-02T03:04:21Z',
      'updatedAt' => '2000-01-02T03:04:22Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_kind_2) do
    {
      'id' => '9cb82c33-48d0-4866-92c6-972cf3c7fe57',
      'code' => 'delivery_kind_code_2',
      'deliveryMethodId' => 'b7849251-4d73-4c70-af00-993fe057505c',
      'adapterId' => '6bbcedf0-ca59-4d51-8118-0fd7ce96ca32',
      'createdAt' => '2000-01-02T03:04:23Z',
      'updatedAt' => '2000-01-02T03:04:24Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method_1) do
    {
      'id' => '3bcc64ff-d711-4444-81ec-5b50e1c41be4',
      'code' => 'delivery_method_code_1',
      'createdAt' => '2000-01-02T03:04:09Z',
      'updatedAt' => '2000-01-02T03:04:10Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method_2) do
    {
      'id' => 'b7849251-4d73-4c70-af00-993fe057505c',
      'code' => 'delivery_method_code_2',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_fields_1) do
    [{
      'id' => 'efe5199c-71b9-4d09-b2eb-0367eb48a867',
      'deliveryId' => '9824ee55-b5a1-44e4-838c-c23f1396a5a0',
      'name' => 'name_1',
      'value' => 'value_1',
      'createdAt' => '2000-01-02T03:04:25Z',
      'updatedAt' => '2000-01-02T03:04:26Z'
    }]
  end

  let(:delivery_fields_2) do
    [
      {
        'id' => 'a1057180-528b-40d0-ae11-55e2d7adade6',
        'deliveryId' => '7f29aa77-6b3d-4dbf-997c-2abfb1a58511',
        'name' => 'name_2',
        'value' => 'value_2',
        'createdAt' => '2000-01-02T03:04:27Z',
        'updatedAt' => '2000-01-02T03:04:28Z'
      },
      {
        'id' => '4498d781-a14e-419a-a0a1-11783529d947',
        'deliveryId' => '7f29aa77-6b3d-4dbf-997c-2abfb1a58511',
        'name' => 'name_3',
        'value' => 'value_3',
        'createdAt' => '2000-01-02T03:04:29Z',
        'updatedAt' => '2000-01-02T03:04:30Z'
      }
    ]
  end

  let(:delivery_fields_3) do
    [{
      'id' => '0d70aafe-560e-4b75-8c53-0958aff5045c',
      'deliveryId' => '017812ee-310d-4410-afcd-ea5fec0e76f8',
      'name' => 'name_4',
      'value' => 'value_4',
      'createdAt' => '2000-01-02T03:04:31Z',
      'updatedAt' => '2000-01-02T03:04:32Z'
    }]
  end

  describe 'GET /messages' do
    subject(:request) { get '/messages' }

    before { messages }

    let(:expected_response_body) do
      [
        {
          'id' => '6c5da6e8-6753-4407-a3e1-cc3e5c9740c1',
          'messageKind' => 'message_kind_code_1',
          'targetMessageable' => 'target_messageable_1',
          'status' => 'unconfirmed',
          'requestTime' => '2000-01-02T03:04:05Z',
          'deliveries' => [
            {
              'deliveryKind' => 'delivery_kind_code_1',
              'externalId' => 'external_id_1',
              'status' => 'undelivered',
              'errorMessage' => 'undelivered_reason_1',
              'deliveryTime' => '2000-01-02T03:04:15Z',
              'deliveryFields' => {
                'name_1' => 'value_1'
              }
            },
            {
              'deliveryKind' => 'delivery_kind_code_2',
              'externalId' => 'external_id_2',
              'status' => 'unconfirmed',
              'deliveryTime' => '2000-01-02T03:04:17Z',
              'deliveryFields' => {
                'name_2' => 'value_2',
                'name_3' => 'value_3'
              }
            }
          ]
        },
        {
          'id' => '10d574f1-b8ab-4d7f-a621-edfd895fa42a',
          'messageKind' => 'message_kind_code_2',
          'targetMessageable' => 'target_messageable_2',
          'status' => 'delivered',
          'requestTime' => '2000-01-02T03:04:07Z',
          'deliveries' => [{
            'deliveryKind' => 'delivery_kind_code_1',
            'externalId' => 'external_id_3',
            'status' => 'delivered',
            'deliveryTime' => '2000-01-02T03:04:19Z',
            'deliveryFields' => {
              'name_4' => 'value_4'
            }
          }]
        },
        {
          'id' => 'a8a7b386-22b1-4142-816b-676a366cf28a',
          'messageKind' => 'message_kind_code_2',
          'targetMessageable' => 'target_messageable_3',
          'status' => 'pending',
          'requestTime' => '2000-01-02T03:04:09Z',
          'deliveries' => []
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /messages' do
    subject(:request) { post '/messages', params: request_body }

    before do
      allow(NATS).to receive(:start).and_yield(nats_instance)
      allow(nats_instance).to receive(:publish)
    end

    let(:nats_instance) { instance_spy(NATS.to_s) }

    let(:request_body) do
      {
        'messageKind' => 'message_kind_code_1',
        'targetMessageable' => 'messageable'
      }
    end

    let(:expected_response_body) do
      {
        'id' => '9cb82c33-48d0-4866-92c6-972cf3c7fe57',
        'messageKind' => 'message_kind_code_1',
        'targetMessageable' => 'messageable',
        'status' => 'pending',
        'requestTime' => Time.zone.now.iso8601,
        'deliveries' => []
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end

    it 'queues the message id to the `deliveries` messaging queue' do
      request

      expect(nats_instance).to have_received(:publish)
        .with('deliveries', '{"messageId":"9cb82c33-48d0-4866-92c6-972cf3c7fe57"}')
    end
  end

  describe 'GET /messages/{id}' do
    subject(:request) { get "/messages/#{message_id}" }

    let(:message)    { message_2     }
    let(:message_id) { message['id'] }

    let(:expected_response_body) do
      {
        'id' => '10d574f1-b8ab-4d7f-a621-edfd895fa42a',
        'messageKind' => 'message_kind_code_2',
        'targetMessageable' => 'target_messageable_2',
        'status' => 'delivered',
        'requestTime' => '2000-01-02T03:04:07Z',
        'deliveries' => [{
          'deliveryKind' => 'delivery_kind_code_1',
          'externalId' => 'external_id_3',
          'status' => 'delivered',
          'deliveryTime' => '2000-01-02T03:04:19Z',
          'deliveryFields' => {
            'name_4' => 'value_4'
          }
        }]
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end
end
