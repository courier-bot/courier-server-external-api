# frozen_string_literal: true

RSpec.describe DeliveryMethodsController, type: :request do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_delivery_methods)
      .and_return([delivery_method_1, delivery_method_2])

    allow(internal_api_service).to receive(:create_delivery_method).with(anything) do |attrs|
      delivery_method_1.merge(attrs).merge(
        'id' => '594cb4f2-3fd1-496e-8468-889233496e7c',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      )
    end

    allow(internal_api_service).to receive(:retrieve_delivery_method_by_code)
      .with('delivery_method_code_1').and_return('id' => '1ae838a3-382e-4cb8-b240-b4c1748c0a07')

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('1ae838a3-382e-4cb8-b240-b4c1748c0a07').and_return(delivery_method_1)

    allow(internal_api_service).to receive(:retrieve_delivery_method_by_code)
      .with('delivery_method_code_2').and_return('id' => '641ed184-4a75-4a8b-81b4-597b7afbc8d7')

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('641ed184-4a75-4a8b-81b4-597b7afbc8d7').and_return(delivery_method_2)

    allow(internal_api_service).to receive(:update_delivery_method) do |_delivery_method_id, attrs|
      delivery_method_1.merge(attrs).merge('updatedAt' => Time.zone.now.iso8601)
    end

    allow(internal_api_service).to receive(:delete_delivery_method)
      .with('1ae838a3-382e-4cb8-b240-b4c1748c0a07')
  end

  let(:instance) { described_class.new }

  let(:delivery_method_1) do
    {
      'id' => '1ae838a3-382e-4cb8-b240-b4c1748c0a07',
      'code' => 'delivery_method_code_1',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method_2) do
    {
      'id' => '641ed184-4a75-4a8b-81b4-597b7afbc8d7',
      'code' => 'delivery_method_code_2',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z',
      'deletedAt' => nil
    }
  end

  describe 'GET /deliveryMethods' do
    subject(:request) { get '/deliveryMethods' }

    before do
      delivery_method_1
      delivery_method_2
    end

    let(:expected_response_body) do
      [
        {
          'id' => '1ae838a3-382e-4cb8-b240-b4c1748c0a07',
          'code' => 'delivery_method_code_1',
          'updateTime' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => '641ed184-4a75-4a8b-81b4-597b7afbc8d7',
          'code' => 'delivery_method_code_2',
          'updateTime' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /deliveryMethods' do
    subject(:request) { post '/deliveryMethods', params: request_body }

    let(:request_body) do
      {
        'code' => 'new_code'
      }
    end

    let(:expected_response_body) do
      {
        'id' => anything,
        'code' => 'new_code',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'GET /deliveryMethods/{code}' do
    subject(:request) { get "/deliveryMethods/#{requested_code}" }

    let(:requested_code) { delivery_method_1['code'] }

    let(:expected_response_body) do
      {
        'id' => '1ae838a3-382e-4cb8-b240-b4c1748c0a07',
        'code' => 'delivery_method_code_1',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveryMethods/{code}' do
    subject(:request) { put "/deliveryMethods/#{requested_code}", params: request_body }

    let(:requested_code) { delivery_method_1['code'] }

    let(:request_body) do
      {
      }
    end

    let(:expected_response_body) do
      {
        'id' => '1ae838a3-382e-4cb8-b240-b4c1748c0a07',
        'code' => 'delivery_method_code_1',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'DELETE /deliveryMethods/{code}' do
    subject(:request) { delete "/deliveryMethods/#{requested_code}" }

    let(:requested_code) { delivery_method_1['code'] }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end
  end
end
