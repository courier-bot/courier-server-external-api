# frozen_string_literal: true

RSpec.describe MessageKindsController, type: :request do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_message_kinds)
      .and_return([message_kind_1, message_kind_2])

    allow(internal_api_service).to receive(:create_message_kind).with(anything) do |attrs|
      message_kind_1.merge(attrs).merge(
        'id' => 'd271549d-3977-4b97-9d84-a9de0dcb9ba9',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      )
    end

    allow(internal_api_service).to receive(:retrieve_message_kind_by_code)
      .with('message_kind_code_1').and_return('id' => '77d72eef-f601-438f-8a7d-975afd542e67')

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('77d72eef-f601-438f-8a7d-975afd542e67').and_return(message_kind_1)

    allow(internal_api_service).to receive(:retrieve_message_kind_by_code)
      .with('message_kind_code_2').and_return('id' => 'a103b6b1-685f-43ff-81c3-b805cf21c09d')

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('a103b6b1-685f-43ff-81c3-b805cf21c09d').and_return(message_kind_2)

    allow(internal_api_service).to receive(:update_message_kind) do |_message_kind_id, attrs|
      message_kind_1.merge(attrs).merge('updatedAt' => Time.zone.now.iso8601)
    end

    allow(internal_api_service).to receive(:delete_message_kind)
      .with('77d72eef-f601-438f-8a7d-975afd542e67')

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('d271549d-3977-4b97-9d84-a9de0dcb9ba9').and_return(adapter_1)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('c2badc84-0457-4fa8-8b81-694b01031ee9').and_return(adapter_2)

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_1').and_return(adapter_1)

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_2').and_return(adapter_2)
  end

  let(:instance) { described_class.new }

  let(:message_kind_1) do
    {
      'id' => '77d72eef-f601-438f-8a7d-975afd542e67',
      'code' => 'message_kind_code_1',
      'adapterId' => 'd271549d-3977-4b97-9d84-a9de0dcb9ba9',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:message_kind_2) do
    {
      'id' => 'a103b6b1-685f-43ff-81c3-b805cf21c09d',
      'code' => 'message_kind_code_2',
      'adapterId' => 'c2badc84-0457-4fa8-8b81-694b01031ee9',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z',
      'deletedAt' => nil
    }
  end

  let(:adapter_1) do
    {
      'id' => 'd271549d-3977-4b97-9d84-a9de0dcb9ba9',
      'code' => 'adapter_code_1',
      'uri' => 'example.com/adapter_1',
      'createdAt' => '2000-01-02T03:04:09Z',
      'updatedAt' => '2000-01-02T03:04:10Z',
      'deletedAt' => nil
    }
  end

  let(:adapter_2) do
    {
      'id' => 'c2badc84-0457-4fa8-8b81-694b01031ee9',
      'code' => 'adapter_code_2',
      'uri' => 'example.com/adapter_2',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z',
      'deletedAt' => nil
    }
  end

  describe 'GET /messageKinds' do
    subject(:request) { get '/messageKinds' }

    before do
      message_kind_1
      message_kind_2
    end

    let(:expected_response_body) do
      [
        {
          'id' => '77d72eef-f601-438f-8a7d-975afd542e67',
          'code' => 'message_kind_code_1',
          'adapter' => 'adapter_code_1',
          'updateTime' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => 'a103b6b1-685f-43ff-81c3-b805cf21c09d',
          'code' => 'message_kind_code_2',
          'adapter' => 'adapter_code_2',
          'updateTime' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /messageKinds' do
    subject(:request) { post '/messageKinds', params: request_body }

    let(:request_body) do
      {
        'code' => 'new_code',
        'adapter' => 'adapter_code_1'
      }
    end

    let(:expected_response_body) do
      {
        'id' => anything,
        'code' => 'new_code',
        'adapter' => 'adapter_code_1',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'GET /messageKinds/{code}' do
    subject(:request) { get "/messageKinds/#{requested_code}" }

    let(:requested_code) { message_kind_1['code'] }

    let(:expected_response_body) do
      {
        'id' => '77d72eef-f601-438f-8a7d-975afd542e67',
        'code' => 'message_kind_code_1',
        'adapter' => 'adapter_code_1',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /messageKinds/{code}' do
    subject(:request) { put "/messageKinds/#{requested_code}", params: request_body }

    let(:requested_code) { message_kind_1['code'] }

    let(:request_body) do
      {
        adapter: 'adapter_code_2'
      }
    end

    let(:expected_response_body) do
      {
        'id' => '77d72eef-f601-438f-8a7d-975afd542e67',
        'code' => 'message_kind_code_1',
        'adapter' => 'adapter_code_2',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'DELETE /messageKinds/{code}' do
    subject(:request) { delete "/messageKinds/#{requested_code}" }

    let(:requested_code) { message_kind_1['code'] }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end
  end
end
