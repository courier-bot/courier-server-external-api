# frozen_string_literal: true

RSpec.describe AdaptersController, type: :request do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_adapters).and_return([adapter_1, adapter_2])

    allow(internal_api_service).to receive(:create_adapter).with(anything) do |attrs|
      adapter_1.merge(attrs).merge(
        'id' => '5c2c0a9a-60cc-44c3-9136-cb1c67320d10',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      )
    end

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_1').and_return('id' => 'cc3917cc-370f-43fd-8a6c-0fcd89712fac')

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('cc3917cc-370f-43fd-8a6c-0fcd89712fac').and_return(adapter_1)

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_2').and_return('id' => 'e5aba62d-cdad-43a6-a971-29f04d1d858c')

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('e5aba62d-cdad-43a6-a971-29f04d1d858c').and_return(adapter_2)

    allow(internal_api_service).to receive(:update_adapter) do |_adapter_id, attrs|
      adapter_1.merge(attrs).merge('updatedAt' => Time.zone.now.iso8601)
    end

    allow(internal_api_service).to receive(:delete_adapter)
      .with('cc3917cc-370f-43fd-8a6c-0fcd89712fac')
  end

  let(:instance) { described_class.new }

  let(:adapter_1) do
    {
      'id' => 'cc3917cc-370f-43fd-8a6c-0fcd89712fac',
      'code' => 'adapter_code_1',
      'uri' => 'example.com/adapter_1',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:adapter_2) do
    {
      'id' => 'e5aba62d-cdad-43a6-a971-29f04d1d858c',
      'code' => 'adapter_code_2',
      'uri' => 'example.com/adapter_2',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z',
      'deletedAt' => nil
    }
  end

  describe 'GET /adapters' do
    subject(:request) { get '/adapters' }

    before do
      adapter_1
      adapter_2
    end

    let(:expected_response_body) do
      [
        {
          'id' => 'cc3917cc-370f-43fd-8a6c-0fcd89712fac',
          'code' => 'adapter_code_1',
          'uri' => 'example.com/adapter_1',
          'updateTime' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => 'e5aba62d-cdad-43a6-a971-29f04d1d858c',
          'code' => 'adapter_code_2',
          'uri' => 'example.com/adapter_2',
          'updateTime' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /adapters' do
    subject(:request) { post '/adapters', params: request_body }

    let(:request_body) do
      {
        'code' => 'new_code',
        'uri' => 'new_uri'
      }
    end

    let(:expected_response_body) do
      {
        'id' => anything,
        'code' => 'new_code',
        'uri' => 'new_uri',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'GET /adapters/{code}' do
    subject(:request) { get "/adapters/#{requested_code}" }

    let(:requested_code) { adapter_1['code'] }

    let(:expected_response_body) do
      {
        'id' => 'cc3917cc-370f-43fd-8a6c-0fcd89712fac',
        'code' => 'adapter_code_1',
        'uri' => 'example.com/adapter_1',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /adapters/{code}' do
    subject(:request) { put "/adapters/#{requested_code}", params: request_body }

    let(:requested_code) { adapter_1['code'] }

    let(:request_body) do
      {
        uri: 'updated_uri'
      }
    end

    let(:expected_response_body) do
      {
        'id' => 'cc3917cc-370f-43fd-8a6c-0fcd89712fac',
        'code' => 'adapter_code_1',
        'uri' => 'updated_uri',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'DELETE /adapters/{code}' do
    subject(:request) { delete "/adapters/#{requested_code}" }

    let(:requested_code) { adapter_1['code'] }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end
  end
end
