# frozen_string_literal: true

RSpec.describe DeliveryKindsController, type: :request do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_delivery_kinds)
      .and_return([delivery_kind_1, delivery_kind_2])

    allow(internal_api_service).to receive(:create_delivery_kind).with(anything) do |attrs|
      delivery_kind_1.merge(attrs).merge(
        'id' => 'c16f152a-0db2-4fbf-89f0-0a624f02a102',
        'createdAt' => Time.zone.now.iso8601,
        'updatedAt' => Time.zone.now.iso8601
      )
    end

    allow(internal_api_service).to receive(:retrieve_delivery_kind_by_code)
      .with('delivery_kind_code_1').and_return('id' => '44bca042-8520-472f-916c-87753c33a272')

    allow(internal_api_service).to receive(:retrieve_delivery_kind)
      .with('44bca042-8520-472f-916c-87753c33a272').and_return(delivery_kind_1)

    allow(internal_api_service).to receive(:retrieve_delivery_kind_by_code)
      .with('delivery_kind_code_2').and_return('id' => 'd3070685-15b3-41b6-8472-ca098cf084a7')

    allow(internal_api_service).to receive(:retrieve_delivery_kind)
      .with('d3070685-15b3-41b6-8472-ca098cf084a7').and_return(delivery_kind_2)

    allow(internal_api_service).to receive(:update_delivery_kind) do |_delivery_kind_id, attrs|
      delivery_kind_1.merge(attrs).merge('updatedAt' => Time.zone.now.iso8601)
    end

    allow(internal_api_service).to receive(:delete_delivery_kind)
      .with('44bca042-8520-472f-916c-87753c33a272')

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('a49d6385-a0e5-4b8b-a59a-b5e500d44855').and_return(delivery_method_1)

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('abd9e5a2-a898-419c-9b54-9d1ccd29e50e').and_return(delivery_method_2)

    allow(internal_api_service).to receive(:retrieve_delivery_method_by_code)
      .with('delivery_method_code_1').and_return(delivery_method_1)

    allow(internal_api_service).to receive(:retrieve_delivery_method_by_code)
      .with('delivery_method_code_2').and_return(delivery_method_2)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('d41e19d3-4fb1-453b-a939-63caa90f0c49').and_return(adapter_1)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('198ff120-e215-4e51-a908-07342467de9a').and_return(adapter_2)

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_1').and_return(adapter_1)

    allow(internal_api_service).to receive(:retrieve_adapter_by_code)
      .with('adapter_code_2').and_return(adapter_2)
  end

  let(:instance) { described_class.new }

  let(:delivery_kind_1) do
    {
      'id' => '44bca042-8520-472f-916c-87753c33a272',
      'code' => 'delivery_kind_code_1',
      'deliveryMethodId' => 'a49d6385-a0e5-4b8b-a59a-b5e500d44855',
      'adapterId' => 'd41e19d3-4fb1-453b-a939-63caa90f0c49',
      'createdAt' => '2000-01-02T03:04:05Z',
      'updatedAt' => '2000-01-02T03:04:06Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_kind_2) do
    {
      'id' => 'd3070685-15b3-41b6-8472-ca098cf084a7',
      'code' => 'delivery_kind_code_2',
      'deliveryMethodId' => 'abd9e5a2-a898-419c-9b54-9d1ccd29e50e',
      'adapterId' => '198ff120-e215-4e51-a908-07342467de9a',
      'createdAt' => '2000-01-02T03:04:07Z',
      'updatedAt' => '2000-01-02T03:04:08Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method_1) do
    {
      'id' => 'a49d6385-a0e5-4b8b-a59a-b5e500d44855',
      'code' => 'delivery_method_code_1',
      'createdAt' => '2000-01-02T03:04:09Z',
      'updatedAt' => '2000-01-02T03:04:10Z',
      'deletedAt' => nil
    }
  end

  let(:delivery_method_2) do
    {
      'id' => 'abd9e5a2-a898-419c-9b54-9d1ccd29e50e',
      'code' => 'delivery_method_code_2',
      'createdAt' => '2000-01-02T03:04:11Z',
      'updatedAt' => '2000-01-02T03:04:12Z',
      'deletedAt' => nil
    }
  end

  let(:adapter_1) do
    {
      'id' => 'd41e19d3-4fb1-453b-a939-63caa90f0c49',
      'code' => 'adapter_code_1',
      'createdAt' => '2000-01-02T03:04:13Z',
      'updatedAt' => '2000-01-02T03:04:14Z',
      'deletedAt' => nil
    }
  end

  let(:adapter_2) do
    {
      'id' => '198ff120-e215-4e51-a908-07342467de9a',
      'code' => 'adapter_code_2',
      'createdAt' => '2000-01-02T03:04:15Z',
      'updatedAt' => '2000-01-02T03:04:16Z',
      'deletedAt' => nil
    }
  end

  describe 'GET /deliveryKinds' do
    subject(:request) { get '/deliveryKinds' }

    before do
      delivery_kind_1
      delivery_kind_2
    end

    let(:expected_response_body) do
      [
        {
          'id' => '44bca042-8520-472f-916c-87753c33a272',
          'code' => 'delivery_kind_code_1',
          'deliveryMethod' => 'delivery_method_code_1',
          'adapter' => 'adapter_code_1',
          'updateTime' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => 'd3070685-15b3-41b6-8472-ca098cf084a7',
          'code' => 'delivery_kind_code_2',
          'deliveryMethod' => 'delivery_method_code_2',
          'adapter' => 'adapter_code_2',
          'updateTime' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'POST /deliveryKinds' do
    subject(:request) { post '/deliveryKinds', params: request_body }

    let(:request_body) do
      {
        'code' => 'new_code',
        'deliveryMethod' => 'delivery_method_code_2',
        'adapter' => 'adapter_code_2'
      }
    end

    let(:expected_response_body) do
      {
        'id' => anything,
        'code' => 'new_code',
        'deliveryMethod' => 'delivery_method_code_2',
        'adapter' => 'adapter_code_2',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 201 CREATED' do
      request

      expect(response).to have_http_status(:created)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'GET /deliveryKinds/{code}' do
    subject(:request) { get "/deliveryKinds/#{requested_code}" }

    let(:requested_code) { delivery_kind_1['code'] }

    let(:expected_response_body) do
      {
        'id' => '44bca042-8520-472f-916c-87753c33a272',
        'code' => 'delivery_kind_code_1',
        'deliveryMethod' => 'delivery_method_code_1',
        'adapter' => 'adapter_code_1',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'PUT|PATCH /deliveryKinds/{code}' do
    subject(:request) { put "/deliveryKinds/#{requested_code}", params: request_body }

    let(:requested_code) { delivery_kind_1['code'] }

    let(:request_body) do
      {
        'deliveryMethod' => 'delivery_method_code_2',
        'adapter' => 'adapter_code_2'
      }
    end

    let(:expected_response_body) do
      {
        'id' => '44bca042-8520-472f-916c-87753c33a272',
        'code' => 'delivery_kind_code_1',
        'deliveryMethod' => 'delivery_method_code_2',
        'adapter' => 'adapter_code_2',
        'updateTime' => Time.zone.now.iso8601
      }
    end

    it 'responds with an HTTP 200 OK' do
      request

      expect(response).to have_http_status(:ok)
    end

    it 'responds with the correct body' do
      request

      response_body = JSON.parse(response.body)
      expect(response_body).to match(expected_response_body)
    end
  end

  describe 'DELETE /deliveryKinds/{code}' do
    subject(:request) { delete "/deliveryKinds/#{requested_code}" }

    let(:requested_code) { delivery_kind_1['code'] }

    it 'responds with an HTTP 204 NO CONTENT' do
      request

      expect(response).to have_http_status(:no_content)
    end
  end
end
