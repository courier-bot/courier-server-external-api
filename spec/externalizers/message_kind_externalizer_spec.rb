# frozen_string_literal: true

RSpec.describe MessageKindExternalizer, type: :externalizer do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('aa498473-ca1b-453c-9764-e1ef396da256').and_return('code' => 'adapter_code')
  end

  let(:instance) { described_class.new }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_message_kind) }

    let(:specified_message_kind) do
      {
        'id' => '401934b3-ea26-4f36-a15e-efe3481c80df',
        'code' => 'message_kind_code',
        'adapterId' => 'aa498473-ca1b-453c-9764-e1ef396da256',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    let(:expected_hash) do
      {
        'id' => '401934b3-ea26-4f36-a15e-efe3481c80df',
        'code' => 'message_kind_code',
        'adapter' => 'adapter_code',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
