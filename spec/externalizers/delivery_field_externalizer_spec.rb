# frozen_string_literal: true

RSpec.describe DeliveryFieldExternalizer, type: :externalizer do
  let(:instance) { described_class.new }

  describe '#externalize_array' do
    subject(:externalizer) { instance.externalize_array(specified_delivery_fields) }

    let(:specified_delivery_fields) do
      [
        {
          'id' => 'a8b9bc28-350b-4ba7-ab1d-8bf06a3917a0',
          'deliveryId' => 'c83f0f37-8cfe-47b0-bb9e-6dddd94c5073',
          'name' => 'field_name_1',
          'value' => 'field_value_1',
          'createdAt' => '2000-01-02T03:04:05Z',
          'updatedAt' => '2000-01-02T03:04:06Z'
        },
        {
          'id' => '7fef599d-b41a-42b9-82b0-7baf67892ab7',
          'deliveryId' => 'ee2cd945-b7da-4981-aa88-f2e95c9d3cdd',
          'name' => 'field_name_2',
          'value' => 'field_value_2',
          'createdAt' => '2000-01-02T03:04:07Z',
          'updatedAt' => '2000-01-02T03:04:08Z'
        }
      ]
    end

    let(:expected_hash) do
      {
        'field_name_1' => 'field_value_1',
        'field_name_2' => 'field_value_2'
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
