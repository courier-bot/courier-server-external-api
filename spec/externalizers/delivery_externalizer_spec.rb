# frozen_string_literal: true

RSpec.describe DeliveryExternalizer, type: :externalizer do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    delivery_field_externalizer = instance_double(DeliveryFieldExternalizer)
    allow(DeliveryFieldExternalizer).to receive(:new).and_return(delivery_field_externalizer)

    delivery_fields = instance_double(Hash)

    allow(internal_api_service).to receive(:retrieve_delivery_fields_by_delivery_id)
      .with('0a3129a9-abc6-4865-8b39-9dd776238742').and_return(delivery_fields)

    allow(delivery_field_externalizer).to receive(:externalize_array)
      .with(delivery_fields).and_return(externalized_delivery_fields)

    allow(internal_api_service).to receive(:retrieve_delivery_kind)
      .with('a1ec4a34-ca02-4ed2-9cb0-0412cded984f').and_return('code' => 'delivery_kind_code')
  end

  let(:instance) { described_class.new }

  let(:externalized_delivery_fields) { instance_double(Hash) }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_delivery) }

    let(:specified_delivery) do
      {
        'id' => '0a3129a9-abc6-4865-8b39-9dd776238742',
        'messageId' => '58fde1f0-b887-4059-a315-fee253e21120',
        'deliveryKindId' => 'a1ec4a34-ca02-4ed2-9cb0-0412cded984f',
        'externalId' => 'external_id',
        'status' => 'delivered',
        'undeliveredReason' => nil,
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z'
      }
    end

    let(:expected_hash) do
      {
        'deliveryKind' => 'delivery_kind_code',
        'externalId' => 'external_id',
        'status' => 'delivered',
        'deliveryTime' => '2000-01-02T03:04:05Z',
        'deliveryFields' => externalized_delivery_fields
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end

    context 'when the delivery has an error message' do
      before { specified_delivery['undeliveredReason'] = 'undelivered_reason' }

      it 'contains the `errorMessage` field' do
        expect(externalizer['errorMessage']).to eq('undelivered_reason')
      end
    end
  end
end
