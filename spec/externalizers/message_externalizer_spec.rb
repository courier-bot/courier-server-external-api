# frozen_string_literal: true

RSpec.describe MessageExternalizer, type: :externalizer do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    delivery_externalizer = instance_double(DeliveryExternalizer)
    allow(DeliveryExternalizer).to receive(:new).and_return(delivery_externalizer)

    deliveries = instance_double(Hash)

    allow(internal_api_service).to receive(:retrieve_deliveries_by_message_id)
      .with('4bc336d5-7866-4c21-bf13-59df0963198b').and_return(deliveries)

    allow(delivery_externalizer).to receive(:externalize_array)
      .with(deliveries).and_return(externalized_deliveries)

    allow(internal_api_service).to receive(:retrieve_message_kind)
      .with('6963d53e-2c7d-40ce-81fa-cd1b61b0aa4f').and_return('code' => 'message_kind_code')
  end

  let(:instance) { described_class.new }

  let(:externalized_deliveries) { instance_double(Hash) }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_message) }

    let(:specified_message) do
      {
        'id' => '4bc336d5-7866-4c21-bf13-59df0963198b',
        'messageKindId' => '6963d53e-2c7d-40ce-81fa-cd1b61b0aa4f',
        'targetMessageable' => 'target_messageable',
        'status' => 'delivered',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z'
      }
    end

    let(:expected_hash) do
      {
        'id' => '4bc336d5-7866-4c21-bf13-59df0963198b',
        'messageKind' => 'message_kind_code',
        'targetMessageable' => 'target_messageable',
        'status' => 'delivered',
        'requestTime' => '2000-01-02T03:04:05Z',
        'deliveries' => externalized_deliveries
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
