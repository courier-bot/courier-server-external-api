# frozen_string_literal: true

RSpec.describe DeliveryKindExternalizer, type: :externalizer do
  before do
    internal_api_service = instance_double(InternalApiService)
    allow(InternalApiService).to receive(:new).and_return(internal_api_service)

    allow(internal_api_service).to receive(:retrieve_delivery_method)
      .with('ae41ba53-8d9a-41c8-b3eb-be3da7670b41').and_return('code' => 'delivery_method_code')

    allow(internal_api_service).to receive(:retrieve_adapter)
      .with('369ca108-60d9-4d91-a0a8-dc4c5083392b').and_return('code' => 'adapter_code')
  end

  let(:instance) { described_class.new }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_delivery_kind) }

    let(:specified_delivery_kind) do
      {
        'id' => 'fece31b8-155d-480a-81df-4c7dc71b80bd',
        'code' => 'delivery_kind_code',
        'deliveryMethodId' => 'ae41ba53-8d9a-41c8-b3eb-be3da7670b41',
        'adapterId' => '369ca108-60d9-4d91-a0a8-dc4c5083392b',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => nil
      }
    end

    let(:expected_hash) do
      {
        'id' => 'fece31b8-155d-480a-81df-4c7dc71b80bd',
        'code' => 'delivery_kind_code',
        'deliveryMethod' => 'delivery_method_code',
        'adapter' => 'adapter_code',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
