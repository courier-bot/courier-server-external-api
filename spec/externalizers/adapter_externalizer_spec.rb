# frozen_string_literal: true

RSpec.describe AdapterExternalizer, type: :externalizer do
  let(:instance) { described_class.new }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_adapter) }

    let(:specified_adapter) do
      {
        'id' => '11d577d7-b8c3-447b-8f18-75612249a1cc',
        'code' => 'adapter_code',
        'uri' => 'example.com/adapter',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => '2000-01-02T03:04:07Z'
      }
    end

    let(:expected_hash) do
      {
        'id' => '11d577d7-b8c3-447b-8f18-75612249a1cc',
        'code' => 'adapter_code',
        'uri' => 'example.com/adapter',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
