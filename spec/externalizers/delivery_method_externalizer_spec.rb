# frozen_string_literal: true

RSpec.describe DeliveryMethodExternalizer, type: :externalizer do
  let(:instance) { described_class.new }

  describe '#externalize' do
    subject(:externalizer) { instance.externalize(specified_delivery_method) }

    let(:specified_delivery_method) do
      {
        'id' => '767ef2d6-187c-42ac-8632-afaa624be212',
        'code' => 'delivery_method_code',
        'createdAt' => '2000-01-02T03:04:05Z',
        'updatedAt' => '2000-01-02T03:04:06Z',
        'deletedAt' => nil
      }
    end

    let(:expected_hash) do
      {
        'id' => '767ef2d6-187c-42ac-8632-afaa624be212',
        'code' => 'delivery_method_code',
        'updateTime' => '2000-01-02T03:04:06Z'
      }
    end

    it 'returns the correct hash' do
      expect(externalizer).to match(expected_hash)
    end
  end
end
