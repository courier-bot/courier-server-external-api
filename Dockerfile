FROM ruby:2.6.3-buster

WORKDIR /project/

RUN gem install bundler \
  rubocop rubocop-md rubocop-performance rubocop-rails rubocop-rspec \
  rubycritic roodi fasterer \
  brakeman bundler-audit --no-document

COPY Gemfile Gemfile.lock /project/
RUN bundle install

COPY . /project/

# RUN useradd -m courier-server
# USER courier-server

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
