# frozen_string_literal: true

require_relative 'boot'

require 'rails'
require 'action_controller/railtie'

Bundler.require(*Rails.groups)

module ExternalApi
  # Main module of the application.
  class Application < Rails::Application
    config.load_defaults 5.2

    Rails.logger = Logger.new("log/#{Rails.env}.log")
    Rails.logger.level = Logger::INFO

    logger = ActiveSupport::Logger.new("log/#{Rails.env}.log")
    logger.formatter = ::Logger::Formatter.new
    config.log_level = :info
    config.log_tags = [:request_id]
    config.logger = ActiveSupport::TaggedLogging.new(logger)
  end
end
