# frozen_string_literal: true

Rails.application.routes.draw do
  resources :messages, only: %i[index create show]
  match '/messages', to: 'messages#options', via: :options
  match '/messages/:id', to: 'messages#option', via: :options

  resources :message_kinds,
            path: 'messageKinds',
            param: :code,
            only: %i[index create show update destroy]
  match '/messageKinds', to: 'message_kinds#options', via: :options
  match '/messageKinds/:code', to: 'message_kinds#option', via: :options

  resources :adapters,
            param: :code,
            only: %i[index create show update destroy]
  match '/adapters', to: 'adapters#options', via: :options
  match '/adapters/:code', to: 'adapters#option', via: :options

  resources :delivery_kinds,
            path: 'deliveryKinds',
            param: :code,
            only: %i[index create show update destroy]
  match '/deliveryKinds', to: 'delivery_kinds#options', via: :options
  match '/deliveryKinds/:code', to: 'delivery_kinds#option', via: :options

  resources :delivery_methods,
            path: 'deliveryMethods',
            param: :code,
            only: %i[index create show update destroy]
  match '/deliveryMethods', to: 'delivery_methods#options', via: :options
  match '/deliveryMethods/:code', to: 'delivery_methods#option', via: :options
end
