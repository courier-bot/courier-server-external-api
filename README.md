# README

The External API is one of the entry points of the Courier Server, offering an API to create new
messages and configure data.

## DEVELOPMENT

NATS information can be displayed by opening a browser to `http://localhost:8222`.

Furthermore, what passes through the messaging queue can be dumped in a terminal with:
```
docker-compose exec external-api nats-sub deliveries -s nats://nats_client:password@messaging-queue:4222
```
