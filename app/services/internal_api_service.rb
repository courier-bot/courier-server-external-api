# frozen_string_literal: true

# Service to connect with the Courier Server's Internal API.
class InternalApiService
  attr_reader :base_uri

  include InternalApi::MessageService
  include InternalApi::DeliveryService
  include InternalApi::DeliveryFieldService
  include InternalApi::MessageKindService
  include InternalApi::AdapterService
  include InternalApi::DeliveryKindService
  include InternalApi::DeliveryMethodService

  # Initializes a new instance of a service to communicate with the Internal API.
  def initialize
    host = ENV['INTERNAL_API_HOST']
    port = ENV['INTERNAL_API_PORT']

    @base_uri = "http://#{host}:#{port}"
  end

  private

  # Performs a GET request to the specified path.
  #
  # @param path [String] Path endpoint.
  def perform_get_request(path)
    Faraday.get(
      "#{base_uri}#{path}",
      nil,
      'Accept' => 'application/json'
    )
  end

  # Performs a POST request to the specified path.
  #
  # @param path [String] Path endpoint.
  # @param body [Hash] Optional request body.
  def perform_post_request(path, body = nil)
    Faraday.post(
      "#{base_uri}#{path}",
      body,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    )
  end

  # Performs a PATCH request to the specified path.
  #
  # @param path [String] Path endpoint.
  # @param body [Hash] Optional request body.
  def perform_patch_request(path, body = nil)
    Faraday.patch(
      "#{base_uri}#{path}",
      body,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    )
  end

  # Performs a DELETE request to the specified path.
  #
  # @param path [String] Path endpoint.
  def perform_delete_request(path)
    Faraday.delete(
      "#{base_uri}#{path}"
    )
  end
end
