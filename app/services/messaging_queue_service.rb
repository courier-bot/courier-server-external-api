# frozen_string_literal: true

require 'nats/client'

# Service to manage a queue of messages.
class MessagingQueueService
  attr_reader :base_uri

  # Initializes a new instance of a service to manage a queue of messages.
  def initialize
    host = ENV['MESSAGING_QUEUE_HOST']
    port = ENV['MESSAGING_QUEUE_PORT']
    user = ENV['MESSAGING_QUEUE_USER']
    password = ENV['MESSAGING_QUEUE_PASSWORD']
    @base_uri = "nats://#{user}:#{password}@#{host}:#{port}"
  end

  # Queues the specified hash to the `deliveries` messaging queue.
  def queue_delivery(delivery_fields)
    opts = {
      servers: [base_uri]
    }

    NATS.start(opts) do |nats_client|
      nats_client.publish('deliveries', delivery_fields.to_json) do
        nats_client.close
      end
    end
  end
end
