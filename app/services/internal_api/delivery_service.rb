# frozen_string_literal: true

module InternalApi
  # Grouping of delivery services to connect with the Courier Server's Internal API.
  module DeliveryService
    # Retrieves all deliveries for a specified message.
    #
    # @param message_id [String] Id of the message.
    def retrieve_deliveries_by_message_id(message_id)
      response = perform_get_request("/deliveries?messageId=#{message_id}")

      JSON.parse(response.body)
    end
  end
end
