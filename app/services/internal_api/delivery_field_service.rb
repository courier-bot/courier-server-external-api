# frozen_string_literal: true

module InternalApi
  # Grouping of delivery field services to connect with the Courier Server's Internal API.
  module DeliveryFieldService
    # Retrieves all delivery fields for a specified delivery.
    #
    # @param delivery_id [String] Id of the delivery.
    def retrieve_delivery_fields_by_delivery_id(delivery_id)
      response = perform_get_request("/deliveryFields?deliveryId=#{delivery_id}")

      JSON.parse(response.body)
    end
  end
end
