# frozen_string_literal: true

module InternalApi
  # Grouping of delivery kind services to connect with the Courier Server's Internal API.
  module DeliveryKindService
    # Retrieves all delivery kinds.
    def retrieve_delivery_kinds
      response = perform_get_request('/deliveryKinds')

      JSON.parse(response.body)
    end

    # Creates a new delivery kind with the specified attributes.
    #
    # @param attrs [Hash] Attributes to set.
    def create_delivery_kind(attrs)
      response = perform_post_request(
        '/deliveryKinds',
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Retrieves the delivery kind with the specified id.
    #
    # @param delivery_kind_id [String] Id of the delivery kind.
    def retrieve_delivery_kind(delivery_kind_id)
      response = perform_get_request("/deliveryKinds/#{delivery_kind_id}")

      JSON.parse(response.body)
    end

    # Retrieves the delivery kind with the specified code.
    #
    # @param code [String] Code value of the delivery kind.
    def retrieve_delivery_kind_by_code(code)
      path = "/deliveryKinds?code=#{code}"
      response = perform_get_request(path)

      JSON.parse(response.body).first
    end

    # Updates a delivery kind with the specified attributes.
    #
    # @param delivery_kind_id [String] Id of the delivery kind.
    # @param attrs [Hash] Attributes to set.
    def update_delivery_kind(delivery_kind_id, attrs)
      response = perform_patch_request(
        "/deliveryKinds/#{delivery_kind_id}",
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Deletes the delivery kind with the specified id.
    #
    # @param delivery_kind_id [String] Id of the delivery kind.
    def delete_delivery_kind(delivery_kind_id)
      perform_delete_request(
        "/deliveryKinds/#{delivery_kind_id}"
      )
    end
  end
end
