# frozen_string_literal: true

module InternalApi
  # Grouping of delivery method services to connect with the Courier Server's Internal API.
  module DeliveryMethodService
    # Retrieves all delivery methods.
    def retrieve_delivery_methods
      response = perform_get_request('/deliveryMethods')

      JSON.parse(response.body)
    end

    # Creates a new delivery method with the specified attributes.
    #
    # @param attrs [Hash] Attributes to set.
    def create_delivery_method(attrs)
      response = perform_post_request(
        '/deliveryMethods',
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Retrieves the delivery method with the specified id.
    #
    # @param delivery_method_id [String] Id of the delivery method.
    def retrieve_delivery_method(delivery_method_id)
      response = perform_get_request("/deliveryMethods/#{delivery_method_id}")

      JSON.parse(response.body)
    end

    # Retrieves the delivery method with the specified code.
    #
    # @param code [String] Code value of the delivery method.
    def retrieve_delivery_method_by_code(code)
      path = "/deliveryMethods?code=#{code}"
      response = perform_get_request(path)

      JSON.parse(response.body).first
    end

    # Updates a delivery method with the specified attributes.
    #
    # @param delivery_method_id [String] Id of the delivery method.
    # @param attrs [Hash] Attributes to set.
    def update_delivery_method(delivery_method_id, attrs)
      response = perform_patch_request(
        "/deliveryMethods/#{delivery_method_id}",
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Deletes the delivery method with the specified id.
    #
    # @param delivery_method_id [String] Id of the delivery method.
    def delete_delivery_method(delivery_method_id)
      perform_delete_request(
        "/deliveryMethods/#{delivery_method_id}"
      )
    end
  end
end
