# frozen_string_literal: true

module InternalApi
  # Grouping of message services to connect with the Courier Server's Internal API.
  module MessageService
    # Retrieves all messages.
    def retrieve_messages
      response = perform_get_request('/messages')

      JSON.parse(response.body)
    end

    # Creates a new message with the specified attributes.
    #
    # @param attrs [Hash] Attributes to set.
    def create_message(attrs)
      response = perform_post_request(
        '/messages',
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Retrieves the message with the specified id.
    #
    # @param message_id [String] Id of the message.
    def retrieve_message(message_id)
      response = perform_get_request("/messages/#{message_id}")

      JSON.parse(response.body)
    end
  end
end
