# frozen_string_literal: true

module InternalApi
  # Grouping of message kind services to connect with the Courier Server's Internal API.
  module MessageKindService
    # Retrieves all message kinds.
    def retrieve_message_kinds
      response = perform_get_request('/messageKinds')

      JSON.parse(response.body)
    end

    # Creates a new message kind with the specified attributes.
    #
    # @param attrs [Hash] Attributes to set.
    def create_message_kind(attrs)
      response = perform_post_request(
        '/messageKinds',
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Retrieves the message kind with the specified id.
    #
    # @param message_kind_id [String] Id of the message kind.
    def retrieve_message_kind(message_kind_id)
      response = perform_get_request("/messageKinds/#{message_kind_id}")

      JSON.parse(response.body)
    end

    # Retrieves the message kind with the specified code.
    #
    # @param code [String] Code value of the message kind.
    def retrieve_message_kind_by_code(code)
      path = "/messageKinds?code=#{code}"
      response = perform_get_request(path)

      JSON.parse(response.body).first
    end

    # Updates a message kind with the specified attributes.
    #
    # @param message_kind_id [String] Id of the message kind.
    # @param attrs [Hash] Attributes to set.
    def update_message_kind(message_kind_id, attrs)
      response = perform_patch_request(
        "/messageKinds/#{message_kind_id}",
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Deletes the message kind with the specified id.
    #
    # @param message_kind_id [String] Id of the message kind.
    def delete_message_kind(message_kind_id)
      perform_delete_request(
        "/messageKinds/#{message_kind_id}"
      )
    end
  end
end
