# frozen_string_literal: true

module InternalApi
  # Grouping of adapter services to connect with the Courier Server's Internal API.
  module AdapterService
    # Retrieves all adapters.
    def retrieve_adapters
      response = perform_get_request('/adapters')

      JSON.parse(response.body)
    end

    # Creates a new adapter with the specified attributes.
    #
    # @param attrs [Hash] Attributes to set.
    def create_adapter(attrs)
      response = perform_post_request(
        '/adapters',
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Retrieves the adapter with the specified id.
    #
    # @param adapter_id [String] Id of the adapter.
    def retrieve_adapter(adapter_id)
      response = perform_get_request("/adapters/#{adapter_id}")

      JSON.parse(response.body)
    end

    # Retrieves the adapter with the specified code.
    #
    # @param code [String] Code value of the adapter.
    def retrieve_adapter_by_code(code)
      path = "/adapters?code=#{code}"
      response = perform_get_request(path)

      JSON.parse(response.body).first
    end

    # Updates a adapter with the specified attributes.
    #
    # @param adapter_id [String] Id of the adapter.
    # @param attrs [Hash] Attributes to set.
    def update_adapter(adapter_id, attrs)
      response = perform_patch_request(
        "/adapters/#{adapter_id}",
        attrs.to_json
      )

      JSON.parse(response.body)
    end

    # Deletes the adapter with the specified id.
    #
    # @param adapter_id [String] Id of the adapter.
    def delete_adapter(adapter_id)
      perform_delete_request(
        "/adapters/#{adapter_id}"
      )
    end
  end
end
