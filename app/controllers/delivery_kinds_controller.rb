# frozen_string_literal: true

# Handles requests related to delivery kinds.
class DeliveryKindsController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /deliveryKinds` requests to retrieve multiple delivery kinds.
  def index
    delivery_kinds = internal_api_service.retrieve_delivery_kinds
    delivery_kinds = externalizer.externalize_array(delivery_kinds)

    render json: delivery_kinds
  end

  # Handles `POST /deliveryKinds` requests to create a new delivery kind.
  def create
    delivery_kind_params = params.permit(:code, :deliveryMethod, :adapter)

    delivery_method_code = delivery_kind_params.delete('deliveryMethod')
    delivery_kind_params['deliveryMethodId'] =
      internalize_delivery_method_code(delivery_method_code)

    adapter_code = delivery_kind_params.delete('adapter')
    delivery_kind_params['adapterId'] = internalize_adapter_code(adapter_code)

    delivery_kind = internal_api_service.create_delivery_kind(delivery_kind_params)
    delivery_kind = externalizer.externalize(delivery_kind)

    response.set_header('Location', "#{request.url}/#{delivery_kind.fetch('id')}")

    render json: delivery_kind,
           status: :created
  end

  # Handles `GET /deliveryKinds/:code` requests to retrieve an existing delivery kind.
  def show
    delivery_kind = internal_api_service.retrieve_delivery_kind(delivery_kind_id)
    delivery_kind = externalizer.externalize(delivery_kind)

    render json: delivery_kind
  end

  # Handles `PUT/PATCH /deliveryKinds/:code` requests to update an existing delivery kind.
  def update
    delivery_kind_params = params.permit(:deliveryMethod, :adapter)

    delivery_method_code = delivery_kind_params.delete('deliveryMethod')
    adapter_code = delivery_kind_params.delete('adapter')

    if delivery_method_code
      delivery_kind_params['deliveryMethodId'] =
        internalize_delivery_method_code(delivery_method_code)
    end

    if adapter_code
      delivery_kind_params['adapterId'] =
        internalize_adapter_code(adapter_code)
    end

    delivery_kind = internal_api_service.update_delivery_kind(
      delivery_kind_id,
      delivery_kind_params
    )

    delivery_kind = externalizer.externalize(delivery_kind)

    render json: delivery_kind
  end

  # Handles `DELETE /deliveryKinds/:code` requests to destroy an existing delivery kind.
  def destroy
    internal_api_service.delete_delivery_kind(delivery_kind_id)
  end

  private

  def internal_api_service
    InternalApiService.new
  end

  def externalizer
    DeliveryKindExternalizer.new
  end

  def delivery_kind_id
    code = params.require(:code)
    internal_api_service.retrieve_delivery_kind_by_code(code).fetch('id')
  end

  def internalize_delivery_method_code(delivery_method_code)
    delivery_method = internal_api_service.retrieve_delivery_method_by_code(delivery_method_code)
    delivery_method.fetch('id')
  end

  def internalize_adapter_code(adapter_code)
    adapter = internal_api_service.retrieve_adapter_by_code(adapter_code)
    adapter.fetch('id')
  end
end
