# frozen_string_literal: true

# Handles requests related to adapters.
class AdaptersController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /adapters` requests to retrieve multiple adapters.
  def index
    adapters = internal_api_service.retrieve_adapters
    adapters = externalizer.externalize_array(adapters)

    render json: adapters
  end

  # Handles `POST /adapters` requests to create a new adapter.
  def create
    adapter_params = params.permit(:code, :uri)

    adapter = internal_api_service.create_adapter(adapter_params)
    adapter = externalizer.externalize(adapter)

    response.set_header('Location', "#{request.url}/#{adapter.fetch('id')}")

    render json: adapter,
           status: :created
  end

  # Handles `GET /adapters/:code` requests to retrieve an existing adapter.
  def show
    adapter = internal_api_service.retrieve_adapter(adapter_id)
    adapter = externalizer.externalize(adapter)

    render json: adapter
  end

  # Handles `PUT/PATCH /adapters/:code` requests to update an existing adapter.
  def update
    adapter_params = params.permit(:uri)

    adapter = internal_api_service.update_adapter(adapter_id, adapter_params)
    adapter = externalizer.externalize(adapter)

    render json: adapter
  end

  # Handles `DELETE /adapters/:code` requests to destroy an existing adapter.
  def destroy
    internal_api_service.delete_adapter(adapter_id)
  end

  private

  def internal_api_service
    InternalApiService.new
  end

  def externalizer
    AdapterExternalizer.new
  end

  def adapter_id
    code = params.require(:code)
    internal_api_service.retrieve_adapter_by_code(code).fetch('id')
  end
end
