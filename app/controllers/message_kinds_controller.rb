# frozen_string_literal: true

# Handles requests related to message kinds.
class MessageKindsController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /messageKinds` requests to retrieve multiple message kinds.
  def index
    message_kinds = internal_api_service.retrieve_message_kinds
    message_kinds = externalizer.externalize_array(message_kinds)

    render json: message_kinds
  end

  # Handles `POST /messageKinds` requests to create a new message kind.
  def create
    message_kind_params = params.permit(:code, :adapter)

    adapter_code = message_kind_params.delete('adapter')
    message_kind_params['adapterId'] = internalize_adapter_code(adapter_code)

    message_kind = internal_api_service.create_message_kind(message_kind_params)
    message_kind = externalizer.externalize(message_kind)

    response.set_header('Location', "#{request.url}/#{message_kind.fetch('id')}")

    render json: message_kind,
           status: :created
  end

  # Handles `GET /messageKinds/:code` requests to retrieve an existing message kind.
  def show
    message_kind = internal_api_service.retrieve_message_kind(message_kind_id)
    message_kind = externalizer.externalize(message_kind)

    render json: message_kind
  end

  # Handles `PUT/PATCH /messageKinds/:code` requests to update an existing message kind.
  def update
    message_kind_params = params.permit(:adapter)

    adapter_code = message_kind_params.delete('adapter')
    message_kind_params['adapterId'] = internalize_adapter_code(adapter_code) if adapter_code

    message_kind = internal_api_service.update_message_kind(message_kind_id, message_kind_params)
    message_kind = externalizer.externalize(message_kind)

    render json: message_kind
  end

  # Handles `DELETE /messageKinds/:code` requests to destroy an existing message kind.
  def destroy
    internal_api_service.delete_message_kind(message_kind_id)
  end

  private

  def internal_api_service
    @internal_api_service ||= InternalApiService.new
  end

  def externalizer
    MessageKindExternalizer.new
  end

  def message_kind_id
    code = params.require(:code)
    internal_api_service.retrieve_message_kind_by_code(code).fetch('id')
  end

  def internalize_adapter_code(adapter_code)
    adapter = internal_api_service.retrieve_adapter_by_code(adapter_code)
    adapter.fetch('id')
  end
end
