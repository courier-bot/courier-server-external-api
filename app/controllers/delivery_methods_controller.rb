# frozen_string_literal: true

# Handles requests related to delivery methods.
class DeliveryMethodsController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /deliveryMethods` requests to retrieve multiple delivery methods.
  def index
    delivery_methods = internal_api_service.retrieve_delivery_methods
    delivery_methods = externalizer.externalize_array(delivery_methods)

    render json: delivery_methods
  end

  # Handles `POST /deliveryMethods` requests to create a new delivery method.
  def create
    delivery_method_params = params.permit(:code)

    delivery_method = internal_api_service.create_delivery_method(delivery_method_params)
    delivery_method = externalizer.externalize(delivery_method)

    response.set_header('Location', "#{request.url}/#{delivery_method.fetch('id')}")

    render json: delivery_method,
           status: :created
  end

  # Handles `GET /deliveryMethods/:code` requests to retrieve an existing delivery method.
  def show
    delivery_method = internal_api_service.retrieve_delivery_method(delivery_method_id)
    delivery_method = externalizer.externalize(delivery_method)

    render json: delivery_method
  end

  # Handles `PUT/PATCH /deliveryMethods/:code` requests to update an existing delivery method.
  def update
    delivery_method_params = params.permit({})

    delivery_method = internal_api_service.update_delivery_method(
      delivery_method_id,
      delivery_method_params
    )

    delivery_method = externalizer.externalize(delivery_method)

    render json: delivery_method
  end

  # Handles `DELETE /deliveryMethods/:code` requests to destroy an existing delivery method.
  def destroy
    internal_api_service.delete_delivery_method(delivery_method_id)
  end

  private

  def internal_api_service
    InternalApiService.new
  end

  def externalizer
    DeliveryMethodExternalizer.new
  end

  def delivery_method_id
    code = params.require(:code)
    internal_api_service.retrieve_delivery_method_by_code(code).fetch('id')
  end
end
