# frozen_string_literal: true

# Handles requests related to messages.
class MessagesController < ApplicationController
  protect_from_forgery with: :null_session

  # Handles `GET /messages` requests to retrieve multiple messages.
  def index
    messages = internal_api_service.retrieve_messages
    messages = externalizer.externalize_array(messages)

    render json: messages
  end

  # Handles `POST /messages` requests to create a new message.
  def create
    message_params = params.permit(:messageKind, :targetMessageable)

    message_kind_code = message_params.delete('messageKind')
    message_params['messageKindId'] = internalize_message_kind_code(message_kind_code)

    message_params['status'] = 'pending'

    message = internal_api_service.create_message(message_params)
    queue_delivery(message)
    message = externalizer.externalize(message)

    response.set_header('Location', "#{request.url}/#{message.fetch('id')}")

    render json: message,
           status: :created
  end

  # Handles and overrides the default `OPTIONS /messages/:id` requests.
  def option
    headers['Access-Control-Allow-Methods'] = 'OPTIONS, HEAD, GET'
    head :ok
  end

  # Handles `GET /messages/:id` requests to retrieve a message.
  def show
    message = internal_api_service.retrieve_message(message_id)
    message = externalizer.externalize(message)

    render json: message
  end

  private

  def internal_api_service
    InternalApiService.new
  end

  def externalizer
    MessageExternalizer.new
  end

  def message_id
    params.require(:id)
  end

  def internalize_message_kind_code(message_kind_code)
    message_kind = internal_api_service.retrieve_message_kind_by_code(message_kind_code)
    message_kind.fetch('id')
  end

  def queue_delivery(message)
    messaging_queue_service = MessagingQueueService.new
    messaging_queue_service.queue_delivery('messageId' => message.fetch('id'))
  end
end
