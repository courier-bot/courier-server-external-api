# frozen_string_literal: true

# Base controller for the application.
class ApplicationController < ActionController::Base
  before_action :set_default_headers

  API_VERSIONS = %i[v1].freeze
  private_constant :API_VERSIONS

  CURRENT_API_VERSION = API_VERSIONS.last
  private_constant :CURRENT_API_VERSION

  # Handles `OPTIONS /resources` requests.
  def options
    allowed_methods = %w[OPTIONS HEAD GET POST]
    headers['Access-Control-Allow-Methods'] = allowed_methods.join(',')

    head :ok
  end

  # Handles `OPTIONS /resources/:id` requests.
  def option
    allowed_methods = %w[OPTIONS HEAD GET PUT PATCH DELETE]
    headers['Access-Control-Allow-Methods'] = allowed_methods.join(',')

    head :ok
  end

  protected

  # Sets some response headers to allow cross-origin requests.
  # TODO: Only allow cross-origin requests from the Admin UI application.
  def set_default_headers
    response.headers['Content-Version'] = requested_version

    response.headers['Access-Control-Allow-Origin'] = '*'

    allowed_headers = %w[Accept Accept-Version Content-Type Origin]
    response.headers['Access-Control-Allow-Headers'] = allowed_headers.join(',')
  end

  # Finds the request's API version, defaulting to the current one.
  #
  # TODO: Raise exception if version is invalid.
  def requested_version
    version = request.headers['Accept-Version']

    return CURRENT_API_VERSION if version.blank?

    version.to_sym
  end
end
