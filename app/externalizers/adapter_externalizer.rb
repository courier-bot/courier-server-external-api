# frozen_string_literal: true

# Service to convert an adapter as represented in the internal database to and from a representation
# of the adapter that is more humanly readable. This also abstracts attributes that are only
# required internally.
class AdapterExternalizer < BaseExternalizer
  # Receives an adapter hash and converts it to a format that is more useful to external users.
  #
  # @param adapter [Hash] Adapter hash to externalize.
  def externalize(adapter)
    result = {}

    result['id'] = adapter.fetch('id')
    result['code'] = adapter.fetch('code')
    result['uri'] = adapter.fetch('uri')
    result['updateTime'] = adapter.fetch('updatedAt')

    result
  end
end
