# frozen_string_literal: true

# Service to convert a delivery method as represented in the internal database to and from a
# representation of the delivery method that is more humanly readable. This also abstracts
# attributes that are only required internally.
class DeliveryMethodExternalizer < BaseExternalizer
  # Receives a delivery method hash and converts it to a format that is more useful to external
  # users.
  #
  # @param delivery_method [Hash] Delivery method hash to externalize.
  def externalize(delivery_method)
    result = {}

    result['id'] = delivery_method.fetch('id')
    result['code'] = delivery_method.fetch('code')
    result['updateTime'] = delivery_method.fetch('updatedAt')

    result
  end
end
