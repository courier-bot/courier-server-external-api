# frozen_string_literal: true

# Service to convert a message kind as represented in the internal database to and from a
# representation of the message kind that is more humanly readable. This also abstracts attributes
# that are only required internally.
class MessageKindExternalizer < BaseExternalizer
  # Receives a message kind hash and converts it to a format that is more useful to external users.
  #
  # @param message_kind [Hash] Message kind hash to externalize.
  def externalize(message_kind)
    result = {}

    adapter_id = message_kind.fetch('adapterId')

    result['id'] = message_kind.fetch('id')
    result['code'] = message_kind.fetch('code')
    result['adapter'] = fetch_adapter(adapter_id).fetch('code')
    result['updateTime'] = message_kind.fetch('updatedAt')

    result
  end

  private

  def fetch_adapter(adapter_id)
    fetch(:adapters, adapter_id) do |id|
      internal_api_service.retrieve_adapter(id)
    end
  end
end
