# frozen_string_literal: true

# Service to convert a delivery field as represented in the internal database to and from a
# representation of the delivery field that is more humanly readable. This also abstracts attributes
# that are only required internally.
class DeliveryFieldExternalizer < BaseExternalizer
  # Receives an array of delivery field hashes and converts them to a format that is more useful to
  # external users.
  #
  # @param delivery_fields [Array[Hash]] Delivery field hashes to externalize.
  def externalize_array(delivery_fields)
    result = {}

    delivery_fields.each do |delivery_field|
      result[delivery_field.fetch('name')] = delivery_field.fetch('value')
    end

    result
  end
end
