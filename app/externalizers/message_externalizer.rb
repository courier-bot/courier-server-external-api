# frozen_string_literal: true

# Service to convert a message as represented in the internal database to and from a representation
# of the message that is more humanly readable. This also abstracts attributes that are only
# required internally.
class MessageExternalizer < BaseExternalizer
  # Receives a message hash and converts it to a format that is more useful to external users.
  #
  # @param message [Hash] Message hash to externalize.
  def externalize(message)
    result = {}

    message_id = message.fetch('id')
    message_kind_id = message.fetch('messageKindId')

    result['id'] = message_id
    result['messageKind'] = fetch_message_kind(message_kind_id).fetch('code')
    result['targetMessageable'] = message.fetch('targetMessageable')
    result['status'] = message.fetch('status')
    result['requestTime'] = message.fetch('createdAt')
    result['deliveries'] = externalized_deliveries(message_id)

    result
  end

  private

  def fetch_message_kind(message_kind_id)
    fetch(:message_kinds, message_kind_id) do |id|
      internal_api_service.retrieve_message_kind(id)
    end
  end

  def externalized_deliveries(message_id)
    deliveries = internal_api_service.retrieve_deliveries_by_message_id(message_id)
    DeliveryExternalizer.new(cache).externalize_array(deliveries)
  end
end
