# frozen_string_literal: true

# Service to convert a delivery kind as represented in the internal database to and from a
# representation of the delivery kind that is more humanly readable. This also abstracts attributes
# that are only required internally.
class DeliveryKindExternalizer < BaseExternalizer
  # Receives a delivery kind hash and converts it to a format that is more useful to external users.
  #
  # @param delivery_kind [Hash] Delivery kind hash to externalize.
  def externalize(delivery_kind)
    result = {}

    delivery_method_id = delivery_kind.fetch('deliveryMethodId')
    adapter_id = delivery_kind.fetch('adapterId')

    result['id'] = delivery_kind.fetch('id')
    result['code'] = delivery_kind.fetch('code')
    result['deliveryMethod'] = fetch_delivery_method(delivery_method_id).fetch('code')
    result['adapter'] = fetch_adapter(adapter_id).fetch('code')
    result['updateTime'] = delivery_kind.fetch('updatedAt')

    result
  end

  private

  def fetch_delivery_method(delivery_method_id)
    fetch(:delivery_methods, delivery_method_id) do |id|
      internal_api_service.retrieve_delivery_method(id)
    end
  end

  def fetch_adapter(adapter_id)
    fetch(:adapters, adapter_id) do |id|
      internal_api_service.retrieve_adapter(id)
    end
  end
end
