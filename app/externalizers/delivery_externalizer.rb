# frozen_string_literal: true

# Service to convert a delivery as represented in the internal database to and from a
# representation of the delivery that is more humanly readable. This also abstracts attributes that
# are only required internally.
class DeliveryExternalizer < BaseExternalizer
  # Receives a delivery hash and converts it to a format that is more useful to external users.
  #
  # @param delivery [Hash] Delivery hash to externalize.
  def externalize(delivery)
    result = {}

    delivery_id = delivery.fetch('id')
    delivery_kind_id = delivery.fetch('deliveryKindId')

    result['deliveryKind'] = fetch_delivery_kind(delivery_kind_id).fetch('code')
    result['externalId'] = delivery.fetch('externalId')
    result['status'] = delivery.fetch('status')
    result['deliveryTime'] = delivery.fetch('createdAt')

    error_message = delivery.fetch('undeliveredReason')
    result['errorMessage'] = error_message if error_message

    result['deliveryFields'] = externalized_delivery_fields(delivery_id)

    result
  end

  private

  def fetch_delivery_kind(delivery_kind_id)
    fetch(:delivery_kinds, delivery_kind_id) do |id|
      internal_api_service.retrieve_delivery_kind(id)
    end
  end

  def externalized_delivery_fields(delivery_id)
    delivery_fields = internal_api_service.retrieve_delivery_fields_by_delivery_id(delivery_id)
    DeliveryFieldExternalizer.new(cache).externalize_array(delivery_fields)
  end
end
