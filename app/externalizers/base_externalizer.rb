# frozen_string_literal: true

# Base service to convert an object as represented in the internal database to and from a
# representation of the object that is more humanly readable. This also abstracts attributes that
# are only required internally.
class BaseExternalizer
  # Very simple object cache system that lasts for the duration of the service.
  attr_reader :cache

  # Initializes an instance of the externalizer service to setup a simple object cache system.
  def initialize(cache = nil)
    @cache = cache || {}
  end

  # Receives an array of object hashes and converts them to a format that is more useful to external
  # users.
  #
  # @param objects [Hash] Object hashes to externalize.
  def externalize_array(objects)
    objects.map { |object| externalize(object) }
  end

  private

  # Provides a very basic cache system for the duration of the service.
  def fetch(object, object_id)
    cache[object] ||= {}

    result = cache[object][object_id]

    if result.nil?
      result = yield(object_id) if block_given?
      cache[object][object_id] = result
    end

    result
  end

  def internal_api_service
    @internal_api_service ||= InternalApiService.new
  end
end
